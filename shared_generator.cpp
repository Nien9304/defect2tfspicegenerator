#include "struct.h"

void spiceGenerator::writePowerConnection(ostream &os){
	writeLine(os, "power connection");
	os << "Rpower 0 power_ground 0" << endl;
	for(unsigned idx = 0; idx != m_powerList.size(); ++idx){
		os << "Vpower" << idx << " "
			<< m_powerList[idx] << " power_ground DC=Vsupply" << endl;
	}
	os << "Rground0 " << m_groundList[0] << " 0 0" << endl;
	for(unsigned idx = 1; idx != m_groundList.size(); ++idx){
		os << "Rground" << idx
			<< " " << m_groundList[idx - 1]
			<< " " << m_groundList[idx]
			<< " 0" << endl;
	}
	os << endl;
}
void spiceGenerator::writeNetlist(ostream &os){
	writeLine(os, "original netlist");
	m_subckt.writeNetlist(os);
	os << endl;
}
void spiceGenerator::writeAlter(ostream &os){
	UDT_DEFECT_VEC::iterator lastDefectIt;
	writeLine(os, "alter");
	switch(m_fault.m_type){
	case FT_BRIDGE:
	case FT_DUAL:
	//Rbridge node node value
		for(UDT_DEFECT_VEC::iterator defectIt = m_fault.m_defect.begin();
		defectIt != m_fault.m_defect.end(); ++defectIt){
			os << ".alter" << endl;
			os << "Rbridge " 
				<< m_subckt.m_netlist[defectIt->m_location[0]].m_description[1] 
				<< " "
				<< m_subckt.m_netlist[defectIt->m_location[0]].m_description[2] 
				<< " "
				<< m_valueName
				<< endl;
		}
		break;
	case FT_OPEN:
	//Rname_last node node original_value
	//Rname_current node node value
		lastDefectIt = m_fault.m_defect.end();
		for(UDT_DEFECT_VEC::iterator defectIt = m_fault.m_defect.begin();
		defectIt != m_fault.m_defect.end(); ++defectIt){
			os << ".alter" << endl;
			if(lastDefectIt != m_fault.m_defect.end()){
				os << "R";
				for(UDT_STR_VEC::iterator strIt = m_subckt.
				m_netlist[lastDefectIt->m_location[0]].m_description.begin();
				strIt != m_subckt.
				m_netlist[lastDefectIt->m_location[0]].m_description.end(); ++strIt){
					os << *strIt << " ";
				}
				os << endl;
			}
			os << "R" 
				<< m_subckt.m_netlist[defectIt->m_location[0]].m_description[0] 
				<< " "
				<< m_subckt.m_netlist[defectIt->m_location[0]].m_description[1] 
				<< " "
				<< m_subckt.m_netlist[defectIt->m_location[0]].m_description[2] 
				<< " "
				<< m_valueName
				<< endl;
			lastDefectIt = defectIt;
		}
		break;
	case FT_TON:
	//Rbridge node node value
		for(UDT_DEFECT_VEC::iterator defectIt = m_fault.m_defect.begin();
		defectIt != m_fault.m_defect.end(); ++defectIt){
			os << ".alter" << endl;
			os << "Rbridge " 
				<< m_subckt.m_netlist[defectIt->m_location[0]].m_description[1] //D
				<< " "
				<< m_subckt.m_netlist[defectIt->m_location[0]].m_description[3] //S
				<< " "
				<< m_valueName
				<< endl;
		}
		break;
	case FT_TOFF:
	//Mname_last ...
	//Mname_current 0 node 0 ... 
	//Ropen node node value
		lastDefectIt = m_fault.m_defect.end();
		for(UDT_DEFECT_VEC::iterator defectIt = m_fault.m_defect.begin();
		defectIt != m_fault.m_defect.end(); ++defectIt){
			os << ".alter" << endl;
			if(lastDefectIt != m_fault.m_defect.end()){
				for(UDT_STR_VEC::iterator strIt = m_subckt.
				m_netlist[lastDefectIt->m_location[0]].m_description.begin();
				strIt != m_subckt.
				m_netlist[lastDefectIt->m_location[0]].m_description.end(); ++strIt){
					os << *strIt << " ";
				}
				os << endl;
			}
			for(unsigned index = 0; index <
			m_subckt.m_netlist[defectIt->m_location[0]]
			.m_description.size(); ++index){
				if(index == 1 || index == 3){
					os << "0 ";
				}
				else{
					os << m_subckt.m_netlist[defectIt->m_location[0]]
						.m_description[index] << " ";
				}
			}
			os << endl;
			os << "Ropen " 
				<< m_subckt.m_netlist[defectIt->m_location[0]].m_description[1] //D
				<< " "
				<< m_subckt.m_netlist[defectIt->m_location[0]].m_description[3] //S
				<< " "
				<< m_valueName
				<< endl;
			lastDefectIt = defectIt;
		}
		break;
	}
}
void spiceGenerator::writeEnd(ostream &os){
	writeLine(os, "END");
	os << ".end" << endl;
}
void spiceGenerator::writeLine(ostream &os, const string& text){
	int left, right;
	right = MAX_PAGE_WIDTH - text.size() - 2;
	left = right/2;
	right -= left;
	os << "*";
	for(int i=0; i<left; ++i){
		os << LINE_SEP;
	}
	os << ' ';
	os << text;
	os << ' ';
	for(int i=0; i<right; ++i){
		os << LINE_SEP;
	}
	os << endl;	
}