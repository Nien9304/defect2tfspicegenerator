#include "struct_spice.h"

void subckt::readSerialFile(ifstream &ifs){
	string str;
	int num;
	checkId(ifs, "#option");
	ifs >> num;
	m_option.resize(num);
	getline(ifs, str);
	for(UDT_STR_VEC::iterator strIt = m_option.begin(); 
	strIt != m_option.end(); ++strIt){
		getline(ifs, *strIt);
	}
	checkId(ifs, "#description");
	ifs >> num;
	m_description.resize(num);
	for(UDT_STR_VEC::iterator strIt = m_description.begin(); 
	strIt != m_description.end(); ++strIt){
		ifs >> *strIt;
	}
	checkId(ifs, "#element");
	ifs >> num;
	m_netlist.resize(num);
	for(UDT_ELEMENT_VEC::iterator elementIt = m_netlist.begin(); 
	elementIt != m_netlist.end(); ++elementIt){
		ifs >> elementIt->m_type;
		ifs >> num;
		elementIt->m_description.resize(num);
		for(UDT_STR_VEC::iterator strIt = elementIt->m_description.begin(); 
		strIt != elementIt->m_description.end(); ++strIt){
			ifs >> *strIt;
		}
	}
	checkId(ifs, "#EOF");
}
std::istream& operator >>(std::istream &is, ELEMENT_TYPE &type){
	string str;
	is >> str;
	if(str == ELEMENT_TYPE_NAME[ET_R]) type = ET_R;
	else if(str == ELEMENT_TYPE_NAME[ET_C]) type = ET_C;
	else if(str == ELEMENT_TYPE_NAME[ET_D]) type = ET_D;
	else if(str == ELEMENT_TYPE_NAME[ET_M]) type = ET_M;
	else if(str == ELEMENT_TYPE_NAME[ET_STAT]) type = ET_STAT;
	else{
		cerr << "Error: Unknown element type " << str << endl;
	}
	return is;
}
void subckt::writeSerialFile(ostream &os){
	os << "#option" << SEP << m_option.size();
	for(UDT_STR_VEC::iterator str_it = m_option.begin();
	str_it != m_option.end(); ++str_it){
		os << SEP << *str_it << endl;
	}
	os << "#description" << SEP << m_description.size();
	for(vector<string>::iterator strIt = m_description.begin();
	strIt != m_description.end(); ++strIt){
		os << SEP << *strIt;
	}
	os << endl;
	os << "#element" << SEP << m_netlist.size() << endl;
	for(vector<element>::iterator elementIt = m_netlist.begin();
	elementIt != m_netlist.end(); ++elementIt){
		os << ELEMENT_TYPE_NAME[elementIt->m_type]
			<< SEP << elementIt->m_description.size();
		for(vector<string>::iterator strIt = elementIt->m_description.begin();
		strIt != elementIt->m_description.end(); ++strIt){
			os << SEP << *strIt;
		}
		os << endl;
	}
	os << "#EOF" << endl;
}
void subckt::writeNetlist(ostream &os){
	for(UDT_ELEMENT_VEC::iterator elementIt = m_netlist.begin();
	elementIt != m_netlist.end(); ++elementIt){
		switch(elementIt->m_type){
		case ET_M:
			//os << "M"; 
			//Can be M or X, the information is recorded with instance name
			for(UDT_STR_VEC::iterator strIt = elementIt->m_description.begin();
			strIt != elementIt->m_description.end(); ++strIt){
				os << *strIt << " ";
			}
			os << endl;
			break;
		case ET_D:
			os << "D"; 
			for(UDT_STR_VEC::iterator strIt = elementIt->m_description.begin();
			strIt != elementIt->m_description.end(); ++strIt){
				os << *strIt << " ";
			}
			os << endl;
			break;
		case ET_C:
			os << "C";
			for(UDT_STR_VEC::iterator strIt = elementIt->m_description.begin();
			strIt != elementIt->m_description.end(); ++strIt){
				os << *strIt << " ";
			}
			os << endl;
			break;
		case ET_R:
			os << "R";
			for(UDT_STR_VEC::iterator strIt = elementIt->m_description.begin();
			strIt != elementIt->m_description.end(); ++strIt){
				os << *strIt << " ";
			}
			os << endl;
			break;
		case ET_STAT:
			os << ".";
			for(UDT_STR_VEC::iterator strIt = elementIt->m_description.begin();
			strIt != elementIt->m_description.end(); ++strIt){
				os << *strIt << " ";
			}
			os << endl;
			break;
		}
	}
}
void subckt::writeSpiceFile(ostream &os){
	for(UDT_STR_VEC::iterator str_it = m_option.begin();
	str_it != m_option.end(); ++str_it){
		os << ".option " << *str_it << endl;
	}
	os << ".subckt";
	for(UDT_STR_VEC::iterator str_it = m_description.begin();
	str_it != m_description.end(); ++str_it){
		os << " " << *str_it;
	}
	os << endl;
	writeNetlist(os);
	os << ".ends" << endl;
}