#include "struct.h"

void spiceGenerator::writeCombSpice(ostream &os, double load_ratio){
	writeTitle(os);
	writeSpiceSetup(os);
	writePowerConnection(os);
	writeSimulationParameter(os);
	writeSimulationSetup(os);
	writeInputPattern(os);
	writeOutputMeasurement(os, load_ratio);
	writeNetlist(os);
	writeAlter(os);
	writeEnd(os);
}
void spiceGenerator::writeTitle(ostream &os){
	writeLine(os, "description");
	os << "* Usage: 2tf analysis" << endl;
	os << "* Cell: " << m_cell.m_name << endl;
	os << "* Type: " << CELL_TYPE_NAME[m_cell.m_type] << endl;
	os << "* Fault: " << m_faultName << endl;
	os << "* Value: " << m_valueName << endl;
}
void spiceGenerator::writeSpiceSetup(ostream &os){
	writeLine(os, "spice settings");
	os << m_lib;
	if(m_option.size() != 0){
		os << ".option";
	}
	for(unsigned index = 0; index < m_option.size(); ++index){
		os  << " " << m_option[index];
	}
	os << endl;
	os << "*.option post" << endl;
	for(UDT_STR_VEC::iterator strIt = m_subckt.m_option.begin(); 
		strIt != m_subckt.m_option.end(); ++strIt){
		os << ".option" << *strIt << endl;
	}
	os << ".temp " << m_temp << endl;
	os << endl;
}
void spiceGenerator::writeSimulationParameter(ostream &os){
	writeLine(os, "parameter");
	os << ".param Vsupply=" << m_voltage << endl;
	os << ".param Tstart=" << m_Tstart << endl;
	os << ".param Tmeasure=" << m_Tmeasure << endl;
	os << ".param Tdelay=" << m_Tdelay << endl;
	os << ".param Trf=" << m_Trf << endl;
	os << ".param Tperiod='Tdelay+Tmeasure+Trf'" << endl;
	os << endl;
}
void spiceGenerator::writeSimulationSetup(ostream &os){
	writeLine(os, "simulation setup");
	os << ".tran " << m_Tstep << "s 'Tperiod*1' Tstart sweep data=pat" << endl;
	os << endl;
}
void spiceGenerator::writeInputPattern(ostream &os){
	writeLine(os, "input pattern");
	//.param level1_A=0
	for(UDT_STR_VEC::iterator strIt = m_pattern.m_inputPin.begin();
	strIt != m_pattern.m_inputPin.end(); ++strIt){
		os << ".param level1_" << *strIt << "=0" << endl;
		os << ".param level2_" << *strIt << "=0" << endl;
	}
	//.data pat level1_A level2_A level1_B level2_B
	os << ".data pat";
	for(UDT_STR_VEC::iterator strIt = m_pattern.m_inputPin.begin();
	strIt != m_pattern.m_inputPin.end(); ++strIt){
		os << " level1_" << *strIt;
		os << " level2_" << *strIt;
	}
	os << endl;
	//+1 0 1 1
	for(UDT_STR_VEC::iterator strIt = m_pattern.m_transitionTable.begin();
	strIt != m_pattern.m_transitionTable.end(); ++strIt){
		os << "+";
		for(int i = 0; i<m_pattern.m_inputPin.size(); ++i){
			switch((*strIt)[i]){
			case 'L':
				os << "0 0 ";
				break;
			case 'H':
				os << "1 1 ";
				break;
			case 'R':
				os << "0 1 ";
				break;
			case 'F':
				os << "1 0 ";
				break;
			}
		}
		os << endl;
	}
	os << ".enddata" << endl;
	os << endl;
	//--------------------
	//Vin_A  port_A GND 
	//+PULSE('level1_A*Vsupply' 'level2_A*Vsupply' Tdelay Trf Trf Tmeasure 'Tperiod+1')
	//Rport_A port_A A 50
	for(UDT_STR_VEC::iterator strIt = m_pattern.m_inputPin.begin();
	strIt != m_pattern.m_inputPin.end(); ++strIt){
		os << "Vin_" << *strIt << " port_" << *strIt << " 0" << endl;
		os << "+PULSE('level1_" << *strIt << "*Vsupply' 'level2_" << *strIt
			<< "*Vsupply' Tdelay Trf Trf Tmeasure 'Tperiod+1')" << endl;
		os << "Rport_" << *strIt << " port_" << *strIt << " " << *strIt
			<< " " << m_Rinput << endl;
	}
	os << endl;
}
void spiceGenerator::writeOutputMeasurement(ostream &os, double load_ratio){
	writeLine(os, "output measurement");
	//Cload_Z Z GND 10f
	//.meas tran delay_Z
	//+TRIG AT='Tdelay+0.5*Trf'
	//+TARG V(Z) VAL='0.5*Vsupply' CROSS=LAST
	for(UDT_STR_VEC::iterator strIt = m_pattern.m_outputPin.begin();
	strIt != m_pattern.m_outputPin.end(); ++strIt){
		double cload;
		if(m_useVariableCload == true){
			// cload = m_Cload*m_cell.m_drivingStrength;
			cload = m_cell.m_output[*strIt].m_outputMaxCapacitance * load_ratio;
			cload *= 1e15; // normalized to fF (1e-15)
		}
		else{
			cload = m_Cload;
		}
		os << "Cload_" << *strIt << " " << *strIt << " GND " << cload << "f" << endl;
		os << ".meas tran delay_" << *strIt << endl;
		os << "+TRIG AT='Tdelay+0.5*Trf'" << endl;
		os << "+TARG V(" << *strIt << ") VAL='0.5*Vsupply' CROSS=LAST" << endl;
	}
	os << endl;
}

void spiceGenerator::writeLoad(ostream &os, const int POINT_CNT)
{
	os << "#load_cnt " << POINT_CNT << endl;
	os << "#output ";
	for (UDT_STR_VEC::iterator strIt = m_pattern.m_outputPin.begin();
			strIt != m_pattern.m_outputPin.end(); ++strIt) {
		os << *strIt << " ";
	}
	os << endl;
	for(int i = 0; i < POINT_CNT; ++i) {
		double ratio = static_cast<double>(i) / (POINT_CNT-1);
		for (UDT_STR_VEC::iterator strIt = m_pattern.m_outputPin.begin();
			strIt != m_pattern.m_outputPin.end(); ++strIt) {
			double cload;
			if(m_useVariableCload == true) {
				cload = m_cell.m_output[*strIt].m_outputMaxCapacitance * ratio;
			}
			else {
				cload = m_Cload;
			}
			os << cload << " ";
		}
		os << endl;
	}
}
