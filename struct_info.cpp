#include "struct_info.h"

void cell::readSerialInfoFile(istream &is){
	string str;
	int num;
	
	checkId(is, "@name");
	is >> m_name;
	checkId(is, "@info");
	is >> m_footprint;
	is >> m_drivingStrength;
	is >> m_type; //cell type
	checkId(is, "#input");
	is >> num;
	for(int i=0; i<num; ++i){
		is >> str;
		m_input[str].m_name = str;
		is >> m_input[str].m_inputCapacitance;
	}
	checkId(is, "#output");
	is >> num;
	for(int i=0; i<num; ++i){
		is >> str;
		m_output[str].m_name = str;
		is >> m_output[str].m_type; //cell type
		is >> m_output[str].m_outputMaxCapacitance;
	}
	checkId(is, "#clock");
	is >> num;
	if(num == 1){
		is >> m_seq.m_clockPin;
		is >> m_seq.m_clockActivation;
	}
	checkId(is, "#clear");
	is >> num;
	if(num == 1){
		is >> m_seq.m_clearPin;
		is >> m_seq.m_clearActivation;
	}
	checkId(is, "#preset");
	is >> num;
	if(num == 1){
		is >> m_seq.m_presetPin;
		is >> m_seq.m_presetActivation;
	}
	checkId(is, "#clps");
	is >> m_seq.m_clearPresetValueP;
	is >> m_seq.m_clearPresetValueN;
	checkId(is, "#EOF");

}

std::istream& operator >>(std::istream &is, CELL_TYPE &type){
	string str;
	is >> str;
	if(str == CELL_TYPE_NAME[CT_COMB]) type = CT_COMB;
	//else if(str == CELL_TYPE_NAME[CT_FF]) type = CT_FF;
	//else if(str == CELL_TYPE_NAME[CT_LATCH]) type = CT_LATCH;
	else{
		cerr << "Error: Unknown cell type " << str << endl;
		exit(-1);
	}
	return is;
}

void cell::writeSerialInfoFile(ostream &os){
	string str;
	int num;

	os << "@name" << SEP << m_name << endl;
	os << "@info" << SEP << m_footprint
		<< SEP << m_drivingStrength
		<< SEP << CELL_TYPE_NAME[m_type]
		<< endl;
	os << "#input" << SEP << m_input.size() << endl;
	for(UDT_STR_PIN_MAP::const_iterator pinIt = m_input.begin();
	pinIt != m_input.end(); ++pinIt){
		os << pinIt->second.m_name
			<< SEP << pinIt->second.m_inputCapacitance
			<< endl;
	}
	os << "#output" << SEP << m_output.size() << endl;
	for(UDT_STR_PIN_MAP::const_iterator pinIt = m_output.begin();
	pinIt != m_output.end(); ++pinIt){
		os << pinIt->second.m_name << SEP << CELL_TYPE_NAME[m_type]
			<< endl;
	}
	if(m_seq.m_clockPin.empty()){
		os << "#clock" << SEP << "0" << endl;
	}
	else{
		os << "#clock" << SEP << "1"
			<< SEP << m_seq.m_clockPin
			<< SEP << m_seq.m_clockActivation
			<< endl;
	}
	if(m_seq.m_clearPin.empty()){
		os << "#clear" << SEP << "0" << endl;
	}
	else{
		os << "#clear" << SEP << "1"
			<< SEP << m_seq.m_clearPin
			<< SEP << m_seq.m_clearActivation
			<< endl;
	}
	if(m_seq.m_presetPin.empty()){
		os << "#preset" << SEP << "0" << endl;
	}
	else{
		os << "#preset" << SEP << "1"
			<< SEP << m_seq.m_presetPin
			<< SEP << m_seq.m_presetActivation
			<< endl;
	}
	os << "#clps" << SEP << m_seq.m_clearPresetValueP << SEP
		<< m_seq.m_clearPresetValueN << endl;
	os << "#EOF" << endl;
}