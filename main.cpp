const char g_version[] = 
//
//	main.cpp
//	Defect2tfSpiceGenerator
//
//	Created by PCCO	@2016/01/28
"1.3.0"//support any statement(.ic) in subckt
;
#include "struct.h"

int main(int argc, char **argv){
	ofstream ofs;
	ifstream ifs;
	string path;
	string filename;
	spiceGenerator spiceGeneratorInst;

	if(argc != 6){
		cerr << "Version: " << g_version << endl << endl;
		cerr << "Usage ./defect2tfSpiceGenerator "
			 << "[working directory(./)] [cell name] [fault name] [value] [config file]" << endl;
		cerr << "Structure of the working directory:" << endl;
		cerr << "./[fault]/info" << endl;
		cerr << "./[fault]/[value]" << endl;
		cerr << "./info/" << endl;
		cerr << "Files read from the working directory:" << endl;
		cerr << "./info/[cell].libps (cell info file)" << endl;
		cerr << "./info/[cell].1tfpat (1tf pattern file)" << endl;
		cerr << "./info/[cell].2tfpat (2tf pattern file)" << endl;
		cerr << "./info/[cell].spips (spice netlist file)" << endl;
		cerr << "./[fault]/[value]/[cell].2tfdefect (defect file)" << endl;
		cerr << "Files write to the working directory:" << endl;
		cerr << "./[fault]/[value]/[cell].2tf.*.spi (simulation spice)" << endl;
		exit(-1);
	}
	path = argv[1];
	path += '/';
	spiceGeneratorInst.m_cellName = argv[2];
	spiceGeneratorInst.m_faultName = argv[3];
	spiceGeneratorInst.m_valueName = argv[4];
	cout << ">> Read config." << endl;
	filename = argv[5];
	openFile(ifs, filename);
	spiceGeneratorInst.setConfig(ifs);
	ifs.close();
	cout << ">> Read library infomation." << endl;
	filename = path + "info/" + spiceGeneratorInst.m_cellName + ".libps";
	openFile(ifs, filename);
	spiceGeneratorInst.m_cell.readSerialInfoFile(ifs);
	ifs.close();
	cout << ">> Read 1 time frame pattern." << endl;
	filename = path + "info/" + spiceGeneratorInst.m_cellName + ".1tfpat";
	openFile(ifs, filename);
	spiceGeneratorInst.m_pattern.readSerial1tfFile(ifs);
	ifs.close();
	cout << ">> Read 2 time frame pattern." << endl;
	filename = path + "info/" + spiceGeneratorInst.m_cellName + ".2tfpat";
	openFile(ifs, filename);
	spiceGeneratorInst.m_pattern.readSerial2tfFile(ifs);
	ifs.close();
	cout << ">> Read cell spice netlist." << endl;
	filename = path + "info/" + spiceGeneratorInst.m_cellName + ".spips";
	openFile(ifs, filename);
	spiceGeneratorInst.m_subckt.readSerialFile(ifs);
	ifs.close();
	cout << ">> Read defect file." << endl;
	filename = path + spiceGeneratorInst.m_faultName +
		"/" + spiceGeneratorInst.m_valueName +
		"/" + spiceGeneratorInst.m_cellName + ".2tfdefect";
	openFile(ifs, filename);
	spiceGeneratorInst.m_fault.readSerialFile(ifs);
	ifs.close();

	// TODO: add entry in config to specify data point count for Spline interpolation.
	// Currently, 5 points -> 5 SPICE netlists are generated
	cout << ">> Write simulation spice." << endl;
	const int POINT_CNT = 5;
	for(int i = 0; i < POINT_CNT; ++i) {
		double ratio = static_cast<double>(i) / (POINT_CNT-1);
		filename = path + spiceGeneratorInst.m_faultName + "/" + 
			spiceGeneratorInst.m_valueName + "/" + spiceGeneratorInst.m_cellName
			+ ".2tf." + int2str(i) + ".comb.spi";
		openFile(ofs, filename);
		spiceGeneratorInst.writeCombSpice(ofs, ratio);
		ofs.close();
	}
	
	cout << ">> Write output load." << endl;
	ofstream load_lst;
	string load_lst_path = path + spiceGeneratorInst.m_faultName +
		"/" + spiceGeneratorInst.m_valueName +
		"/" + spiceGeneratorInst.m_cellName + ".2tfload";
	openFile(load_lst, load_lst_path);
	spiceGeneratorInst.writeLoad(load_lst, POINT_CNT);
	load_lst.close();

    return 0;
} 
