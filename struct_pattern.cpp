#include "struct_pattern.h"

void pattern::readSerial1tfFile(ifstream &ifs){
	string str;
	int num;
	checkId(ifs, "#input");
	ifs >> num;
	m_inputPin.resize(num);
	checkId(ifs, "#output");
	ifs >> num;
	m_outputPin.resize(num);
	checkId(ifs, "#pattern");
	ifs >> num;
	m_truthTable.resize(num);
	for(UDT_STR_VEC::iterator strIt = m_inputPin.begin(); 
	strIt != m_inputPin.end(); ++strIt){
		ifs >> *strIt;
	}
	for(UDT_STR_VEC::iterator strIt = m_outputPin.begin(); 
	strIt != m_outputPin.end(); ++strIt){
		ifs >> *strIt;
	}
	for(UDT_STR_VEC::iterator strIt = m_truthTable.begin(); 
	strIt != m_truthTable.end(); ++strIt){
		ifs >> *strIt;
	}
	checkId(ifs, "#EOF");
}
void pattern::readSerial2tfFile(ifstream &ifs){
	string str;
	int num;
	checkId(ifs, "#input");
	ifs >> num;
	m_inputPin.resize(num);
	checkId(ifs, "#output");
	ifs >> num;
	m_outputPin.resize(num);
	checkId(ifs, "#pattern");
	ifs >> num;
	m_transitionTable.resize(num);
	for(UDT_STR_VEC::iterator strIt = m_inputPin.begin(); 
	strIt != m_inputPin.end(); ++strIt){
		ifs >> *strIt;
	}
	for(UDT_STR_VEC::iterator strIt = m_outputPin.begin(); 
	strIt != m_outputPin.end(); ++strIt){
		ifs >> *strIt;
	}
	for(UDT_STR_VEC::iterator strIt = m_transitionTable.begin(); 
	strIt != m_transitionTable.end(); ++strIt){
		ifs >> *strIt;
	}
	checkId(ifs, "#EOF");
}

void pattern::writeSerial1tfFile(ostream &os){
	os << "#input" << SEP << m_inputPin.size() << endl;
	os << "#output" << SEP << m_outputPin.size() << endl;
	os << "#pattern" << SEP << m_truthTable.size() << endl;
	for(vector<string>::const_iterator
		strIt = m_inputPin.begin();
		strIt != m_inputPin.end(); ++strIt){
		os << *strIt << SEP;
	}
	for(vector<string>::const_iterator
		strIt = m_outputPin.begin();
		strIt != m_outputPin.end(); ++strIt){
		os << *strIt << SEP;
	}
	os << endl;
	for(vector<string>::const_iterator
		strIt = m_truthTable.begin();
		strIt != m_truthTable.end(); ++strIt){
		os << *strIt << endl;
	}
	os << "#EOF" << endl; ;
}
void pattern::writeSerial2tfFile(ostream &os){
	os << "#input" << SEP << m_inputPin.size() << endl;
	os << "#output" << SEP << m_outputPin.size() << endl;
	os << "#pattern" << SEP << m_transitionTable.size() << endl;
	for(vector<string>::const_iterator
		strIt = m_inputPin.begin();
		strIt !=m_inputPin.end(); ++strIt){
		os << *strIt << SEP;
	}
	for(vector<string>::const_iterator
		strIt = m_outputPin.begin();
		strIt != m_outputPin.end(); ++strIt){
		os << *strIt << SEP;
	}
	os << endl;
	for(vector<string>::const_iterator
		strIt = m_transitionTable.begin();
		strIt != m_transitionTable.end(); ++strIt){
		os << *strIt << endl;
	}
	os << "#EOF" << endl;
}
