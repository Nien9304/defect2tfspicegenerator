#include "struct_defect.h"

void fault::readSerialFile(ifstream &ifs){
	string str;
	int num;
	ifs >> str; //cell name
	ifs >> m_type;
	checkId(ifs, "#defect");
	ifs >> num;
	m_defect.resize(num);
	for(UDT_DEFECT_VEC::iterator defectIt = m_defect.begin(); 
	defectIt != m_defect.end(); ++defectIt){
		ifs >> defectIt->m_name;
		ifs >> num;
		defectIt->m_location.resize(num);
		for(UDT_INT_VEC::iterator intIt = defectIt->m_location.begin();
		intIt != defectIt->m_location.end(); ++intIt){
			ifs >> *intIt;
		}
	}
	checkId(ifs, "#EOF");
}
std::istream& operator >>(std::istream &is, FAULT_TYPE &type){
	string str;
	is >> str;
	if(str == FAULT_TYPE_NAME[FT_BRIDGE]) type = FT_BRIDGE;
	else if(str == FAULT_TYPE_NAME[FT_OPEN]) type = FT_OPEN;
	else if(str == FAULT_TYPE_NAME[FT_TON]) type = FT_TON;
	else if(str == FAULT_TYPE_NAME[FT_TOFF]) type = FT_TOFF;
	else if(str == FAULT_TYPE_NAME[FT_DUAL]) type = FT_DUAL;
	else{
		cerr << "Error: Unknown fault type " << str << endl;
	}
	return is;
}