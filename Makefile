TARGET = defect2tfSpiceGenerator
OBJECT = shared_generator.o generator.o main.o common.o config.o struct_info.o struct_pattern.o struct_spice.o struct_defect.o
CC=g++
CCFLAG=-D LINUX_ENV -std=c++0x -O2
TARGET:$(OBJECT)
	$(CC) $(CCFLAG) -o $(TARGET) $(OBJECT)
DEBUG:CCFLAG=-DLINUX_ENV -std=c++0x -g -Wall
DEBUG:TARGET
#makefile knows the dependency between .o .cpp
struct.h: common.h config.h struct_info.h struct_pattern.h struct_spice.h struct_defect.h
	touch struct.h
common.o:common.cpp common.h
	$(CC) $(CCFLAG) -c common.cpp
config.o:config.cpp config.h
	$(CC) $(CCFLAG) -c config.cpp
struct_info.o:struct_info.cpp struct_info.h
	$(CC) $(CCFLAG) -c struct_info.cpp
struct_pattern.o:struct_pattern.cpp struct_pattern.h
	$(CC) $(CCFLAG) -c struct_pattern.cpp
struct_spice.o:struct_spice.cpp struct_spice.h
	$(CC) $(CCFLAG) -c struct_spice.cpp
struct_defect.o:struct_defect.cpp struct_defect.h
	$(CC) $(CCFLAG) -c struct_defect.cpp
	
shared_generator.o:shared_generator.cpp struct.h
	$(CC) $(CCFLAG) -c shared_generator.cpp
generator.o:generator.cpp struct.h
	$(CC) $(CCFLAG) -c generator.cpp
main.o:main.cpp struct.h
	$(CC) $(CCFLAG) -c main.cpp
clean:
	rm -f $(TARGET) $(OBJECT)
