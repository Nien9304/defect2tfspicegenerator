#ifndef _STRUCT_H_
#define _STRUCT_H_

#include "common.h"
#include "config.h"
#include "struct_info.h"
#include "struct_pattern.h"
#include "struct_spice.h"
#include "struct_defect.h"

struct spiceGenerator{
	spiceGenerator()
		/*:m_temp(25)
		,m_voltage(1.05),m_Tstart(0),m_Tmeasure(5),m_Tdelay(3),m_Trf(0.3),m_Tstep(1)
		,m_Rinput(10000),m_Cload(40),m_useVariableCload(true)*/
	{};
	void setConfig(istream &is){
		m_config.readConfig(is);
		m_config.getCfg(string("spice.lib"), m_lib);
		m_config.getCfg(string("spice.option"), m_option);
		m_config.getCfg(string("spice.temp"), m_temp);
		m_config.getCfg(string("spice.voltage"), m_voltage);
		m_config.getCfg(string("spice.power"), m_powerList);
		m_config.getCfg(string("spice.ground"), m_groundList);
		m_config.getCfg(string("spice.Tstep"), m_Tstep);

		m_config.getCfg(string("spice.Tstart"), m_Tstart);
		m_config.getCfg(string("spice.Tmeasure"), m_Tmeasure);
		m_config.getCfg(string("spice.Tdelay"), m_Tdelay);
		m_config.getCfg(string("spice.Trf"), m_Trf);

		m_config.getCfg(string("spice.Rinput"), m_Rinput);
		m_config.getCfg(string("spice.Cload"), m_Cload);
		m_config.getCfg(string("spice.use_variable_Cload"), m_useVariableCload);
	};

	void writeCombSpice(ostream &os, double load_ratio);
	void writeTitle(ostream &os);
	void writeSpiceSetup(ostream &os);
	void writePowerConnection(ostream &os);
	void writeSimulationParameter(ostream &os);
	void writeSimulationSetup(ostream &os);
	void writeInputPattern(ostream &os);
	void writeOutputMeasurement(ostream &os, double load_ratio);
	void writeNetlist(ostream &os);
	void writeAlter(ostream &os);
	void writeEnd(ostream &os);
	void writeLine(ostream &os, const string& text);
	void writeLoad(ostream &os, const int load_cnt);

	subckt m_subckt;
	cell m_cell;
	pattern m_pattern;
	fault m_fault;
	config m_config;
	UDT_STR_VEC m_setup;
	//scope
	string m_valueName;
	string m_faultName;
	string m_cellName;
	//parameters;
	//setup
	string m_lib;
	UDT_STR_VEC m_option;
	UDT_STR_VEC m_powerList, m_groundList;
	//C
	string m_temp;
	//v
	string m_voltage;
	//ns
	string m_Tstart;
	string m_Tmeasure;
	string m_Tdelay;
	string m_Trf;
	//ps
	string m_Tstep;
	//ohm
	string m_Rinput;
	//fF
	double m_Cload;
	//
	bool m_useVariableCload;
};

#endif